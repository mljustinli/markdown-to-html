let resultsWrapper = document.getElementById("results-wrapper");
let searchInput = document.getElementById("search-input");
searchInput.value = "";

onSearchChange();

function onSearchChange() {
    console.log(searchInput.value);

    let newResults = "";
    let tagSearch = searchInput.value.indexOf("tag:") == 0;
    let tag = searchInput.value.substring(4, searchInput.value.length).trim();

    for (let title in metadata) {
        if (
            (!tagSearch &&
                title
                    .toLowerCase()
                    .includes(searchInput.value.toLowerCase())) ||
            (tagSearch &&
                metadata[title].tags.some((t) => t.includes(tag.toUpperCase())))
        ) {
            newResults +=
                "<div class='result'>" +
                "<div class='note-title'>" +
                "<a href='" +
                title.replace(/ /g, "-") +
                "'>" +
                title +
                "</a>" +
                "</div>" +
                "<div class='note-date'>" +
                "no date" +
                "</div>" +
                "</div>";
        }
    }
    resultsWrapper.innerHTML = newResults;
}
