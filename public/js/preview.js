// https://stackoverflow.com/questions/63894692/efficient-js-event-listener-on-hover

function hover(element, enter, leave) {
    element.addEventListener("mouseenter", enter);
    element.addEventListener("mouseleave", leave);
}

// get reference to content
let contentWrapper = document.getElementById("content");

// create wrapper for iframe
let frameWrapper = document.createElement("div");
frameWrapper.setAttribute("class", "preview-frame");

// create iframe
let iframe = document.createElement("iframe");
frameWrapper.appendChild(iframe);

let links = document.getElementsByTagName("a");

for (let i in links) {
    let link = links[i];

    if (
        link.pathname &&
        link.pathname.includes("/notes") &&
        contentWrapper.contains(link)
    ) {
        hover(
            link,
            (e) => {
                iframe.src = link.pathname;
                link.appendChild(frameWrapper);
                if (
                    link.getBoundingClientRect().top +
                        iframe.getBoundingClientRect().height >
                    window.innerHeight - 20
                ) {
                    frameWrapper.classList.add("preview-frame-pos-up");
                } else {
                    frameWrapper.classList.remove("preview-frame-pos-up");
                }
            },
            (e) => {
                link.removeChild(frameWrapper);
            }
        );
    }
}
