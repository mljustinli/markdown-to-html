let metadataNodes = [];
let metadataLinks = [];

let noteTitles = [];

let linkTags = true;

for (let noteTitle in metadata) {
    noteTitles.push(noteTitle);
}
let titleIndex = 0;
for (let noteTitle in metadata) {
    // add title to nodes
    metadataNodes.push({ title: noteTitle });

    // add links
    // TODO check for duplicates
    metadata[noteTitle].links.forEach((l) => {
        // don't link to self
        if (noteTitle !== l) {
            metadataLinks.push({
                source: titleIndex,
                target: noteTitles.indexOf(l),
                type: "title",
            });
        }
    });

    // add links based on related tags
    if (linkTags) {
        let secondIndex = 0;
        for (let secondNoteTitle in metadata) {
            // don't check against self
            if (noteTitle !== secondNoteTitle) {
                // check if they share a tag
                if (
                    metadata[noteTitle].tags.some((t) =>
                        metadata[secondNoteTitle].tags.includes(t)
                    )
                ) {
                    metadataLinks.push({
                        source: titleIndex,
                        target: secondIndex,
                        type: "tag",
                    });
                }
            }
            secondIndex++;
        }
    }

    titleIndex++;
}

console.log(metadataNodes);
console.log(metadataLinks);

let graph = {
    nodes: metadataNodes,
    links: metadataLinks,
};

console.log(graph.nodes);

const scale = 1.5;
const width = 1200 * scale;
const height = 700 * scale;
const svg = d3.select("svg").attr("viewBox", [0, 0, width, height]);

const centerNode = "welcome to my notes";

const minTextSize = 18;
const maxTextSize = 48;

let mouseX = 3000;
let mouseY = 3000;

let link = svg
    .selectAll(".link")
    .data(graph.links)
    .join("line")
    .classed("link", true)
    .classed("link-by-tag", (d) => d.type === "tag");

let groups = svg
    .selectAll(".node")
    .data(graph.nodes)
    .enter()
    .append("g")
    .attr("r", 24)
    .classed("node", true);

let nodes = groups.append("circle").attr("r", 24);
let labels = groups
    .append("text")
    .text((d) => d.title)
    .on(
        "click",
        (d) => (window.location.href = "notes/" + d.title.replace(/ /g, "-"))
    );

const simulation = d3
    .forceSimulation()
    .nodes(graph.nodes)
    .force(
        "charge",
        // https://stackoverflow.com/a/29164048
        d3
            .forceManyBody()
            .strength((d, i) =>
                d.title === centerNode
                    ? linkTags
                        ? -1300
                        : -800
                    : linkTags
                    ? -180
                    : -40
            )
    )
    .force("center", d3.forceCenter(width / 2, height / 2))
    .force(
        "link",
        d3
            .forceLink(graph.links)
            .distance((d) => (linkTags ? (d.type === "tag" ? 350 : 150) : 150))
            .strength((d) => (d.type === "tag" && linkTags ? 0.1 : 1))
    )
    .force(
        "collision",
        d3.forceCollide().radius((d, i) => 30)
    )
    .on("tick", tick);

function tick() {
    link.attr("x1", (d) => d.source.x)
        .attr("y1", (d) => d.source.y)
        .attr("x2", (d) => d.target.x)
        .attr("y2", (d) => d.target.y);

    groups.attr("transform", (d) => {
        if (d.title === centerNode) {
            d.fx = width / 2;
            d.fy = height / 2;
        }
        return "translate(" + d.x + "," + d.y + ")";
    });

    labels.style("font", (d, i) => calcFontSize(d.x, d.y, i) + "px Poppins");
}

svg.on("mousemove", function () {
    mouseX = d3.mouse(this)[0];
    mouseY = d3.mouse(this)[1];

    // reheat the simulation
    simulation.restart();
});

function calcFontSize(x, y, i) {
    let dist = Math.sqrt(Math.pow(x - mouseX, 2) + Math.pow(y - mouseY, 2));

    // let size = clamp(
    //     0.0002 * Math.pow(dist - 500, 2),
    //     minTextSize,
    //     maxTextSize
    // );
    let size = clamp(2400 / dist, minTextSize, maxTextSize);
    graph.nodes[i].radius = size;

    return size;
}

function clamp(val, min, max) {
    if (val <= max && val >= min) {
        return val;
    } else if (val > max) {
        return max;
    } else {
        return min;
    }
}
