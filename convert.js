const fs = require("fs");
const markdown = require("markdown").markdown;

const notesDir = "notes";
const publicDir = "public/notes";
const createLinksFrom = true;
const useTemplate = true;
const templateKeyword = "\\note";
const templateLinksKeyword = "\\links";

// get all .md file names
const filenames = fs.readdirSync(notesDir).filter((f) => f.includes(".md"));

let metadata = {};

// create metadata structure for all the files first
filenames.forEach((filename) => {
    metadata[filename.split(".md")[0]] = {
        links: [],
        tags: [],
        linksToHere: [],
    };
});

filenames.forEach((filename) => {
    let content = fs.readFileSync(notesDir + "/" + filename, "utf-8");
    // create base HTML files for each markdown file
    createHTML(filename.split(".md")[0], content);
});

if (createLinksFrom) {
    createLinksFromHTML();
}

// create js file with the names of all the files (have to add dashes)
createJSMetadataFile();

// copy all images from note to public folder

function createHTML(filename, content) {
    // search for all occurrences of a file name as long as it's not the original file
    let tempContent = content;

    // remove the first line if it has tags
    let splitIndex = tempContent.indexOf("\n");
    let firstLine = tempContent.substring(0, splitIndex);
    if (firstLine.includes("|")) {
        tempContent = tempContent.substring(splitIndex + 1, tempContent.length);

        let tags = firstLine
            .split("|")
            .map((t) => t.trim().toUpperCase())
            .filter((t) => t !== "");
        metadata[filename].tags = tags;
    }

    // search for references to other files
    filenames.forEach((searchFilename) => {
        let searchMask = searchFilename.split(".md")[0];
        let regEx = new RegExp(searchMask, "ig");

        tempContent = tempContent.replace(regEx, function (match) {
            // save matches between files here
            if (metadata[filename].links.indexOf(searchMask) == -1) {
                metadata[filename].links.push(searchMask);
            }

            if (metadata[searchMask].linksToHere.indexOf(filename) == -1) {
                metadata[searchMask].linksToHere.push(filename);
            }

            // replace search filename with one surrounded with markdown for a link
            return (
                "[" +
                match +
                "](" +
                "../" +
                searchMask.replace(/ /g, "-") +
                "/)"
            );
        });
    });

    let dashFilename = filename.replace(/ /g, "-");

    // create folder if it doesn't exist
    if (!fs.existsSync(publicDir + "/" + dashFilename)) {
        fs.mkdirSync(publicDir + "/" + dashFilename);
    }

    let html = markdown.toHTML(tempContent);

    if (useTemplate) {
        let template = fs.readFileSync(publicDir + "/template.html", "utf-8");
        html = template.replace(templateKeyword, html);
    }

    // create html file
    fs.writeFileSync(
        publicDir + "/" + dashFilename + "/index.html",
        html,
        "utf-8",
        function (err) {
            if (err) {
                console.log("Problem saving html file");
                console.log(err);
                return;
            }
        }
    );
}

function createLinksFromHTML() {
    filenames.forEach((filename) => {
        let dashFilename = filename.split(".md")[0].replace(/ /g, "-");

        let extraMarkdown = "";
        metadata[filename.split(".md")[0]].linksToHere.forEach((link) => {
            extraMarkdown +=
                "- [" + link + "](" + "../" + link.replace(/ /g, "-") + "/)\n";
        });

        let template = fs.readFileSync(
            publicDir + "/" + dashFilename + "/index.html",
            "utf-8"
        );
        template = template.replace(
            templateLinksKeyword,
            markdown.toHTML(extraMarkdown)
        );

        fs.writeFile(
            publicDir + "/" + dashFilename + "/index.html",
            template,
            "utf-8",
            function (err) {
                if (err) {
                    console.log("Problem appending links to files.");
                    console.log(err);
                    return;
                }
            }
        );
    });
}

function createJSMetadataFile() {
    fs.writeFile(
        publicDir.split("/")[0] + "/js/metadata.js",
        "let metadata = " + JSON.stringify(metadata),
        "utf-8",
        function (err) {
            if (err) {
                console.log("Problem saving metadata");
                console.log(err);
                return;
            }
        }
    );
}
