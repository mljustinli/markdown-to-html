computer science | georgia tech |

## People Thread

This is one of eight threads at Georgia Tech that a student can specialize in when doing their bachelor's degree in computer science. It looks at the interactions between people and computers.

My threads in college were the people thread and the media thread.
