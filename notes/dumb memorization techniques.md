advice |

# Dumb Memorization Techniques

Whenever I need to memorize something, I try to find the most spectacularly dumb way of memorizing it.

## Examples
### Korean 
(A lot of these examples are from my trying to memorize Korean phrases. Unlike memorizing words in Spanish, I can't associate Korean phrases with an English counterpart, unless of course it literally is the English word...)

Fast in Korean is 빠른 (bba reun), which sounds like vroom, which is what fast cars do. This is what I think every time I hear or see this adjective now :/

Fat in Korean is 뚱뚱하다 (ddung ddung ha da), which is what it might sound like if you patted a fat cat's belly (dun dun) :D

### Throwing something on the ground

When I was in middle school I read Diary of a Wimpy Kid and in one of the books he mentions throwing a sock on the ground the night before he needs to remember something so that he'll wake up, see the sock, and then remember why he threw it. I sometimes do the same thing but I just throw the tissue box on the ground. Sometimes I still can't remember so... a pen and paper might be better.

### Uhhhhh more like use these memorization techniques to remember the memorization techniques
More to come when I remember...

## In Conclusion
The memorization technique ends up being so random that I remember the weirdness and then I remember the actual meaning. It got me through high school!