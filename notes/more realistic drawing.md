drawing |

# More Realistic Drawing

It's time to learn more about drawing! Maybe this is overly ambitious and I should start with line art though haha.

## To Investigate

- Color dodge
	- Linear dodge
- Multiply layer
- Alpha mask
- Drawing clothes
- Shading clothes D:
- How to draw hair
- How to draw eyes well
- How to draw noses... :/
- Choosing good brushes
- How to draw lips

## Tutorial Channels

- Draw with Jazza
- [Ross Draws](https://invidious.tube/channel/UCLEVrhumRsK67JkP3G4w5cQ)
- Marc Brunet

### Tutorials

- [Styled Portrait Tutorial](https://invidious.tube/watch?v=q9R369KvCXo)
	- Start with cool and warm tones
	- Lighting
	- Can use background color as somewhat of a highlight
	- Color dodge can add energy
	- Linear dodge in "crevices" are cool 
	- Color as an outline/accent
	- Can use lasso tool to create sharp edges (like for shadows)
- [Realistic Portrait Painting](https://invidious.tube/watch?v=j5nksFr29b4)
- Choosing Colors
	- also has tutorial on shading
	- Use colors to emphasize the point of the drawing
		- Usually emphasize their face
	- Choose color that sticks out for emphasis
		- If other colors are less saturated, the emphasized color could be more saturated
	- Color harmonies
		- Complementary
			- Opposite on chromatic circle
		- Triads
			- Evenly spaced on color wheel
	- Color temperature
		- Warm vs Cool
		- Warmer colors for lit areas
		- Cooler colors for shadows
		- See hue shifting!
- Choosing Colors for Skin
	- Quick trick
		- Find reference
		- Filter gallery > Artistic > Cutout 
		- Simplifies into chunks of color, which you can steal for your palette
- [How to Shade a Surface](https://invidious.tube/watch?v=avlQJuKUAKA)
	- Surface is brightest where light "first" hits/more perpendicular to light
	- Some color from background behind the object because some light bounces back
		- Not as bright as the highlight/area that's directly hit by light
	- Casted shadow
		- Darkest color on shape
	- Top lighting?
		- Airbrush to subtly color/tint top of stuff including shadows
	- Simplify shape to a 3D object you can understand
- [Painting Skin](https://invidious.tube/watch?v=vpXQvDWvCNk)
- [Shading Elements](https://invidious.tube/watch?v=K7DseLvcpyY)
	- Solid breakdown of different layers of shading

#### Line Art

- [Line Art Guide](https://invidious.tube/watch?v=ZzgrOMCd380)
	- Line confidence
		- Smoother lines are better than... hairy lines/using multiple small brush strokes to outline
		- Practice using fewer lines
		- Use elbow instead of wrist for longer lines?
	- Line thickness
		- Convergent lines - looks nice to fan out at the base
			- Adds character
		- Details vs Outline
			- Finer line for details
			- Bolder lines for outlines
			- In a way it's like UI design XD bolder lines emphasize important parts of the art and then details add on
		- Also thickness based on depth of object in the drawing
			- Further away = less thick
		- Present or absence of line can suggest where the light is
			- Finer line suggests more light is there
	- Line color
		- Conveys depth
		- Further in back of scene = lighter lines
		- Can change actual colors of lines
			- Tint them with the color of your object
	- Line suggestion
		- Use lines past curve to suggest volume
		- Fade out a fold crease with perpendicular lines
		- Represents fuzzier lines
		- Thinner lines

#### Hair
		
- [Hair Styles](https://invidious.tube/watch?v=tsu7TO44MVI)
	- First determine hairline?
		- Where hair meets forehead
	- Determine where part is 
		- If you don't want a part, then don't draw the guide line
	- Are there any accessories pulling hair together?
		- Hair tie
		- Add a guide line called **gather point**
		- Tucking hair behind ear is also a gather point
	- Curves
		- S curve for long hairs
			- Lighter curve for straighter hair
			- More curve for curlier hair
			- Multiple S's for super duper curly hair
		- C curve
			- Almost straight line vs super curved line
			- Can use for hairs that are sticking out
	-  Follow shape of head and falls on shoulders
	-  Think of hair in bunches/in bulk
		-  S curves that meet up
		-  Two S curves have 4 ways to meet up or not meet up at the top and bottom
	-  Thicker line indicates bulk of hair
	-  Thinner lines indicates detail
	-  Details closer to where S curves meet at the ends
-  [How to Paint Hair](https://invidious.tube/watch?v=39kigEnf6bY)
	-  Start with simple sketch/outline of hair
		-  Rough silhouette
	-  3 Brushes
		-  Standard brush for outline and painting
		-  Air brush for shading
		-  Blender
	-  Rough patch of color for the shape
	-  Use hue shifts to add more color
	-  Focus on prominent shadows
		-  Spontaneous and random places...
		-  Make shadows even darker in some areas
	-  Paint in more saturated strands/details
		-  Closer to bottom?
	-  Add lighting
		-  Yellow hue, brighter
		-  Closer to top
	-  Make shadows EVEN DARKER
		-  Where appropriate
	-  Can use background color and paint in some holes in the hair
	-  Add the brightest highlights over old highlights
	-  But wait add more shadows with some hue variation
	-  Add some loose strands with thinner brush and continuous strokes
	-  Can add even brighter highlights for specular highlights

#### Noses

- [Painting a Nose](https://invidious.tube/watch?v=Bxe4nt5B2-M)
	- From a front view, main line art is towards the bottom of the nose with emphasis on the nostrils
	- Use color to create the narrower part of the nose
	- Use highlights!
	- Start with a horizontal oval?
- [Nose Anatomy](https://invidious.tube/watch?v=_HDyk4dy7kc)
	- Structure
		- Bulb - round part at the bottom of the nose
		- Nostrils - on the sides of bulb
		- Bridge - narrower part of nose
		- Septum - area between nostrils
	- Variation
		- Bulb turns upward
		- Stronger bridge that sticks out
	- Placement
		- Sides of nostrils meet sides of eyes
		- Can help place other anatomy
	- Common mistakes
		- Don't pull nostrils lower than septum
		- Don't make nonstrils too round
		- Bulb should flow into the nostrils?
		- Avoid too many lines
			- If bottom has dark line, top shouldn't have dark line
	- Reduction
		- Hint of nostril usually

#### Eyes

- Random observations
	- Eyelashes tend to be longer/bunched toward the outside of the face
- [Draw Stylized Eyes](https://invidious.tube/watch?v=BfID5va-8NU)
	- Sketch general shape
	- Almondish with iris
	- Iris isn't a complete circle, tends to get cut off at the top
	- Eyebrow arch?
		- Peak is around 45 degrees to the outside?
	- Add skin base color
	- Add shadows
		- Under eyes
		- Top outside
		- Top inside between eyebrow and top of eye
	- Too much shadow under eyes makes them look real tired
	- Eyeball color
		- Off white
	- Shadow over eyeball from eyelid
	- Add iris color
	- Highlights
		- Corner of eye towards nose at tearduct
		- At waterline (line between eye and eyelash at bottom)
		- At eyebrow arc
		- On the eyeball (kawaii)
	- Fine lines
		- Eyebrow hair
		- Eyelashes
		- Can use more crayon/colored pencil texture
		- Like eyeliner
	- Color dodge over eyeball and near tearduct
	- Incorporate into drawing
	- Can use airbrush to add eyeliner/wings
	- Bare minimum
		- Circle
		- Whites
		- Iris color
		- Highlight
	- Other extreme
		- Keep shape data and lose everything else

## Random Tips

- Flip your drawing along the vertical axis every now and then to clearly see what parts are awkward
- Practice lmao
- Stabilizer is great

## Some Takeaways

- Good lineart is really powerful and even terrible coloring can be made good with good lineart
	- Time to LEARN and practice a lot :'
- Thick black/dark lines are good for foreground/emphasizing elements
	- Tinted or thinner lines are good for detail/background elements