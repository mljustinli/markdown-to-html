recipes | korean | advice

## Fillings for Simple Kimbap

Fillings

- Fake crab
- Thin strips of cucumber
- Thin strips of carrot
- Danmuji (the yellow pickled radish)

It tastes really good and it requires less effort!