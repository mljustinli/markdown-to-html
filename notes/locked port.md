command line | web development

## Locked Port - Finding Killing a Process That's Using a Port

```
sudo lsof -i :3000
kill - 9 <PID>
```
