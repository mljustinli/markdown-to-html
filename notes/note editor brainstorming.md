Web Development | JavaScript | Node | API | markdown notes

## Building Your Own WYSIWYG editor

This is some planning for the start of my notes.

WYSIWYG = what you see is what you get

-   basically instead of markdown or html you just see the formatted version like wordpress or a document editor like google docs

Sources

-   [How to build your own WYSIWYG Editor \| by Sai gowtham | codeburst](https://codeburst.io/how-to-build-your-own-wysiwyg-editor-6002fa3f5ea8)
-   quill.js too
-   insert a table
    -   [java - How to create a table in an iframe jquery execCommand? - Stack Overflow](https://stackoverflow.com/questions/38715253/how-to-create-a-table-in-an-iframe-jquery-execcommand)
    -   doesn't actually work lmao
-   saving image from base64
    -   [Convert a base64 string to a file in Node - CodeBlocQ](https://www.codeblocq.com/2016/04/Convert-a-base64-string-to-a-file-in-Node/)
-   packaging into executable
    -   [pkg - npm](https://www.npmjs.com/package/pkg)
    -   `pkg . -t host`
        -   does it for mac
    -   `pkg .`
        -   does it for all 3

Workflow

-   Taking notes for classes
    -   open it quickly (app or bookmarked web page)
    -   associate it with a class folder or tag quickly
    -   just type away really easily
    -   paste screenshots easily/with just pasting
    -   (save automatically/immediately)
-   Taking notes on things you're learning about
    -   open app quickly
    -   associate it with a topic
    -   typity type type
    -   (save automatically)
-   Exploring/visualizing?
-   Links to related articles?
    -   if text in document is the title of another document, hyperlink to it
    -   only calculate after saving once
    -   if you want to recalculate, have a button to do that? otherwise it'd be too laggy lmao idk
    -   or like
    -   option to update all docs with new links to everything else
-   Button to backup to github/upload somewhere (all in public folder I guess)

Features of Editor/Markdown

-   musts
    -   headers
    -   bullet points
    -   image pasting
    -   pasting links creates a hyperlink to the thing
    -   bold, italicize
    -   view all files/filter files
-   maybe
    -   tables
-   areas
    -   markdown
    -

Features of Website

-   exploration of topics (graph)
-   hover links = preview
-   view all files with the same tag
