UI | CSS | Web Development | Responsive Design

## Responsive Design

[Responsive Web Design Grid](https://www.w3schools.com/Css/css_rwd_grid.asp)

-   use grids, usually 12 columns per page on desktop

[Make the Perfect Responsive Grid with CSS - Coder Coder](https://coder-coder.com/make-responsive-grid-css/)

-   can use flexbox!
-   use flex: 1 or flex: 2 to determine the ratio between elements :O

[HTML Responsive Web Design](https://www.w3schools.com/html/html_responsive.asp)

-   set viewport to 1.0
-   the \<picture\> element lets you specify different resolution images based on media queries
