advice | drawing

# Tips for Drawing

In just doing some sketches and copying reference images, I learned a few things that I don't want to forget.

### Find Help

- use references
	- it's way easier to draw clothing folds when you can see it 
- tutorial videos can offer a lot of insight, you just need to be patient and sit through it
	- draw along with the video to really learn
- if you're not sure how to draw a certain body part, search up a tutorial, especially those pinterest or imgur one-page graphic with tips

### Line Art is Good

- line art is very important as the base of the drawing
- line weight and color matter
	- thicker weight might suggest darker areas, and vice versa for thinner lines
	- use thinner lines for details
	- often times lines aren't just black, they might be a darker version of the inside color 
- using thinner brushes looks better imo
- use the full range of tablet pen pressure to get lines that taper at the ends 
- make sure your line art is on the right layer, and if you redo the line art make sure that's on the right layer as well

### Learn the Anatomy
	
- draw a skeleton to help map out proportions and the pose
- apparently a person is about 8 heads tall

### Patience is *the* Virtue

- patience is super important
	- drawing takes a long time!
- remember that for every amazing drawing the artist probably has a ton of horrifying sketches in their sketchbook

### Krita Specific

- the blur tool in Krita (looks like a q-tip) is amazinggggg
- right click on a layer and click on properties to change the blending mode (multiply, color dodge, etc)
- group two layers if you want to use one as an alpha mask (the one matching the alpha mask should have the alpha symbol crossed out)

### Videos That Helped A LOT

- [Draw with Jazza - Anatomy](https://invidious.tube/watch?v=w2fKxNDsXuw)
	- Draw with Jazza in general
- [KNKL - Faces](https://invidious.tube/watch?v=u-NqaB1-F8o)
- Ross Draws in general
- [SomeNormalArtist - Shading and Light](https://invidious.tube/watch?v=K7DseLvcpyY)