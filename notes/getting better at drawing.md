drawing | advice

# Getting Better at Drawing

I'm always amazed by those super detailed digital art paintings and drawings, and I really want to get better at drawing too! Here's kind of my rough plan to get better at drawing.

## Line Art

Based on what I've seen, line art is everything! If you can capture a figure or scene with accurate line art, then it doesn't really matter how well or poorly you color it, because the line art holds it all together. So! The plan is to try drawing a lot and figuring out how to represent objects using lines. Apparently line weight and color matter a lot too so looking at other peoples' drawing can help there.

After I have the basics down and can render good line art, then I'll move on to learning how to color it!

## Look at Colors in References

I never know how to choose colors, so a good approach is probably to use the color picker on a bunch of reference images to get a good idea for what kind of hue, saturation, and brightness people usually use.

## Observation

I noticed that when I draw it ends up looking weird because I don't know how a bicep looks for example. I think for now I'll try to draw an image and that way I don't have to imagine what something looks like, it'll already exist in a reference image. 

## Practice More 

Obviously, practice is super important and I won't get better just by reading, so... I'll draw more.

## Patience

Drawing takes a long time! Even a sketch or line art can take an hour, so drawing and coloring probably takes even longer. By practicing more I can build up stamina and draw for longer and longer, and hopefully as I practice more I'll learn what details to add and how light works.

## So Yeah

We'll see how that goes! My attention span tends to be a week or two so maybe I'll get tired of drawing and pick it up another time, who knows!