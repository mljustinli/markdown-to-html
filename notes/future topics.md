markdown notes |

## Future Topics

Ideas for future notes

- Automating note publishing
- Automating Boostnote notes backup
- Being nervous walking through those store theft detectors for no reason
	- Along those lines, being nervous near cops just because they're cops XD
- Swipe brick breaker neural net
- Search and replace all instances (case insensitive) in JavaScript
- Hue shifting and picking colors
	- More color theory
- Alternatives to Google Products
- How to use vi/emacs editor or at the very least how to use it with git