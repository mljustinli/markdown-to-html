drawing | advice

# Hue Shifting

Based on [this](https://invidious.tube/watch?v=RL_5va7jEx8) video

The idea of hue shifting is that when you're picking a color for shading or highlighting, in addition to changing the brightness and saturation of the color, you also shift the hue towards a cooler or warmer color.

For shadows, make the color less bright and also shift the hue towards a more blue color. For highlights, make the color more bright and shift towards a more red color.

This results in more vibrant palette colors and it's really easy to do!