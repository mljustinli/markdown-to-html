cs 4873 | georgia tech

## Rule Utilitarianism

To justify a stance through the lens of rule utilitarianism, first create or think of a rule that applies for the situation. Then, discuss the pros of having the rule and discuss having the cons of having the rule. If there are more pros than cons, then it's a good rule to follow and actions that follow that rule are ethical.

For example, if the rule is "don't lie," then we could say the pros of this rule are everyone tells the truth and there's no crime (something like that), the cons of this rule are that you might offend some people, and then since the pros outweigh the cons, it is ethical to tell the truth.
