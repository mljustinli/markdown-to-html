Web Development | Firefox

## Disable Caching

Firefox

-   CMD/Ctrl + Shift + R will refresh and reload resources
    -   This generally does the trick
-   Going to Firefox security preferences and managing the cache _should_ work.
-   If all else fails, open the element inspector, go to settings, scroll all the way down, and check "Disable HTTP Cache (when toolbox is open)"
    -   I had some JavaScript file cached while making this website and it was really weird D:
