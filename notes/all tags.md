markdown notes |

## All Tags

Here are some ideas I have for tags...

-   Kubernetes
-   Design
-   Typography
-   UI
-   Java
-   JavaScript
-   Node
-   React
-   Machine Learning
-   Artificial Intelligence
-   Networking
-   Web Development
-   Wordpress
-   Taxes
-   Firefox
-   API
-   Georgia Tech
-   Markdown Notes
-   Advice
-   Drawing
-   Color Theory
