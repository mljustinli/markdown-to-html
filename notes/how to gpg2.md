cryptography | computer science

## GPG 2

[gpg2(1): OpenPGP encryption/signing tool - Linux man page](https://linux.die.net/man/1/gpg2)

Called gpg2

-   Used for encrypting/decrypting files with public keys

How to update

-   gpg2 --gen-key
    -   follow instructions
-   gpg2 --armor --export [your email] > justin-li-public.asc

How to encrypt

-   gpg2 -e --armor some.pdf
    -   --armor makes it an ascii file rather than binary
-   Enter in the recipient email as the id
-   Press enter with a blank line to end

How to decrypt
