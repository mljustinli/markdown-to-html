Web Development | Design

## Welcome to My Notes!

Welcome!

My name is [Justin Li](https://mljustinli.gitlab.io) and these are my random notes. This website is heavily inspired by a [notes website](https://notes.andymatuschak.org/About_these_notes) that my friend showed me. I've been wanting to make something similar for my own use for a while now, and I finally got around to doing it! I think my favorite part is the visualization on the [home](../..) screen. If you're wondering what do the lines mean, check out the linked note.

These are like any other markdown notes but... they're mine so that's cool right? I think the lesson to be learned here is that making your own tools is more fun than finding and using tools online. Although, I couldn't be bothered to make my own markdown to HTML function, so I used [this](https://www.npmjs.com/package/markdown) helpful library. The home page visualization is also made using [D3.js](https://d3js.org/). It was good practice for my CS 4460 data visualization class.

This project is made using Node and various Node libraries. However, the website itself is compiled by my Node program into basic HTML, JS, and CSS files so that loading times are faster!

For better or for worse, the project is public so you can see the code [here](https://gitlab.com/mljustinli/markdown-to-html).

This is still a work in progress, and there are a lot more features I would like to add!