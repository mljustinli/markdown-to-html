computer science | java | streams | functional programming
## Java Streams

- streams
  - functional programming to Java
  - supported in Java 8
  - more efficient, use lambda expressions
  - ParallelStreams make it easy to multi-thread operations
- pipeline
  - source
  - zero or more intermediate operations
  - terminal operation (like forEach)
- Stream source
  - streams created from Collections, Lists, Sets, ints, longs, doubles, arrays, lines of a file
- Stream operations are either intermediate or terminal
  - **Intermediate operations** such as filter, map, or sort return a stream so you can chain multiple
  - **Terminal operations** such as forEach, collect, or reduce are either void or return a non-stream results
- Intermediate Operations
  - order matters for large datasets
  - filter first, and then sort or map
  - use parallelstream to enable multiple threads
  - Intermediate operations
    - anyMatch()
    - distinct
    - filter
    - findFirst
    - flatmap
    - map
    - skip
    - sorted
- Terminal Operations
  - only one allowed
  - forEach applies same function to each elem
  - collect saves the elem into a collection
  - others reduce the stream to a single summary element
    - count
    - max
    - min
    - reduce
    - summaryStatistics
- Examples

``` Java
// prints int 1 to 9 inclusive, doesn't print 10
IntStream
  .range(1,10)
  .forEach(System.out::print);

// prints 6789
IntStream
  .range(1, 10)
  .skip(5) //skip first five elements
  .forEach(x -> System.out.println(x));
  
// prints sum of 1 to 4
System.out.println(
IntStream
  .range(1, 5)
  .sum())
);

// sort and then print first item
Stream.of("Ava", "Aneri", "Alberto")
  .sorted()
  .findFirst()
  .ifPresent(System.out::println);
  
String[] names = {"Al", "Ankit", ...}
Arrays.stream(names) // same as Stream.of(names)
  .filter(x -> x.startsWith("S"))
  .sorted()
  .forEach(System.out::println);
  
// prints average of squares
Arrays.stream(new int[] {2, 4, 6, 8, 10})
  .map(x -> x * x)
  .average()
  .ifPresent(System.out::println);
  
List<String> people = Arrays.asList("Al", ...);
people
  .stream()
  .map(String::toLowerCase)
  .filter(x -> x.startsWith("a"))
  .forEach(System.out::println)
  
// gets each line of a file as a stream of strings
Stream<String> bands = Files.lines(Paths.get("bands.txt"));

bands
  .sorted()
  .filter(x -> x.length() > 13)
  .forEach(System.out::println);
bands.close()

//
List<String> bands2 = Files.lines(Paths.get("bands.txt"))
  .filter(x -> x.contains("jit"))
  .collect(Collectors.toList());
bands2.forEach(x -> System.out.println(x));
```
- 4:19