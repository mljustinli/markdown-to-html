georgia tech | cs 4873

## Ethical Theories

-   **Utilitarianism**
    -   **Act Utilitarianism** - maximize utility
        -   weight pros and cons, say one of them outweighs the other, so do it
    -   **Rule Utilitarianism** - if you universalized the rule, then would it maximize utility?
        -   ex: always follow traffic rules
        -   pros, cons, which on outweights the other
    -   doesn't matter if you pick to do the action or not, just justify your reasoning
-   **Ethical Relativism** - theory that there are no universal moral norms of right and wrong
    -   different individuals and groups can have opposite views and still be right
    -   **Subjective Relativism** - each person decides right and wrong for himself or herself
-   **Cultural Relativism** - meaning of righta nd wrong rests with a society's actual moral guidelines
-   **Kantianism**
    -   don't use people as means to an end
    -   **Imperative** - way in which reason commands the will
        -   **Hypothetical Imperative** - conditional rule of form "if you want X then do Y"
            -   explains means to achieve an end
        -   **Categorical Imperative** - unconditional rule, rule that always applies, regardless of circumstances
            -   imagine if everyone did the same action, would it lead to a contradiction?
            -   ex: if everyone broke promises, promises would have no meaning
            -   regardless of what you want
-   **Social Contract** - civilized society has two things
    -   the establishment of a set of moral rules to govern relations among citizens
    -   government capable of enforcing these rules
