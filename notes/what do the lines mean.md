markdown notes |

## What Do The Lines Mean?

Welcome to my notes!

The lines between the note titles on the home page represent links between related notes. The stronger lines mean that one note references another note's title. The weaker lines mean that the notes share a tag.

If there are too many notes of a single tag then it could get messy since they're all linked together, so I'll have to find a better way of representing it in the future.
