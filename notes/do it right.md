advice |

## Do It Right

When I was in elementary school, I was part of this after-school math team, and my friend once said to me, "If you're going to do a problem, might as well do it right."

In other words, if you're spending time on a math problem, you might as well take as much time as you need and put in the effort to get it right.

I feel like you can extend this to all areas life and say, "If you're going to do something, you might as well do it right." Basically, don't put in half-effort because you might have to revisit the task and redo it anyway as a result of that half-effort. Do it right the first time!

Although... I guess it's easier said than done, and I am also guilty of being lazy and skimming articles hehe.
