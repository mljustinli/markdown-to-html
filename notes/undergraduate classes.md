georgia tech |

## Undergraduate Classes

Here are the classes I took for my CS undergrad! I specialized in the people thread and the media thread.

### Fall 2018
- APPH 1040
- CS 1100
- CS 1331
- ENGL 1101
- MATH 3215
- PUBP 1142

Wow how did I take that many classes at once???

### Spring 2019
- CS 1332
- CS 2340
- ENGL 1102
- MATH 1554

### Fall 2019
- CS 2050
- CS 3750
- CS 2261
- PSYC 1101
- VIP 3601 - RoboSense

### Spring 2020
- CS 3451
- CS 4605
- MATH 3012
- PSYC 2210
- VIP 3602 - RoboSense

### Fall 2020
- CS 3510
- CS 4400
- CS 4455
- VIP 3603 - RoboSense

### Spring 2021
- CS 4460
- CS 4873
- LMC 3403
- PSYC 2015

I have so much writing to do this semester aaaaaaa
