cs 4873 | georgia tech

## Kantianism

Also see deontology.

There are two ways to justify an action with this theory.

1. If you're using people as a means to an end, it is not ethical.

    - For example, if you lie to someone to get food, you use them as a means to get food.

2. If everyone did the same action, and it leads to a contradiction of that rule, then it is not ethical.

    - For example, when deciding whether to break a promise or not, consider what would happen if everyone broke their promises. Promises would have no meaning and this is a contradiction.
