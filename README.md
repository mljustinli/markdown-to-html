## Markdown to HTML

-   Converts markdown to HTML and also replaces all occurrences of file names within other file names with links to those files
-   Confusing right?
-   It's like wikipedia links
-   It doesn't handle note names within note names so well... but it doesn't break at least!

Conventions

-   lowercase title of notes but spaces are fine
-   use first line of notes to put tags, separate with |
    -   if there's only one tag just add a | before or after
-   place a template.html with \note for where you want to insert notes
    -   use \links for where you want to put the "links to here"
